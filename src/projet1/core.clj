(ns projet1.core)
(require '[projet1.util :as util])

(defn -main [& args]
  (if (seq args)
    (cond
      (= (Integer/parseInt (first args)) 1) (do
                                              (println "Mode joueur sélectionné. Taille du code ?")
                                              (let [code (util/code-secret (Integer/parseInt (read-line)))]
                                                (loop [e (util/read-code (count code)), cpt 1]
                                                  (if (= code e)
                                                    (println "Gagné (" cpt "essais)")
                                                    (do
                                                      (println "Indication : " (util/filtre-indications code e (util/indications code e)))
                                                      (recur (util/read-code (count code)) (inc cpt)))))))
      (= (Integer/parseInt (first args)) 2) (do
                                              (println "Mode solveur auto sélectionné.")
                                              (let [code (util/read-code 0)]
                                                (loop [e (util/code-secret (count code)), pool (util/newpool (count code)), cpt 1]
                                                  (println "Essai " cpt " : " e)
                                                  (if (= code e)
                                                    (println "Gagné (" cpt "essais)")
                                                    (let [[pool, e] (util/solve pool e (util/filtre-indications code e (util/indications code e)))]
                                                      (recur e pool (inc cpt)))))))
      (= (Integer/parseInt (first args)) 3) (do
                                              (println "Mode solveur à indications manuelles sélectionné.")
                                              (let [code (util/read-code 0)]
                                                (loop [e (util/code-secret (count code)), pool (util/newpool (count code)), cpt 1]
                                                  (println "Essai " cpt " : " e)
                                                  (if (= code e)
                                                    (println "Gagné (" cpt "essais)")
                                                    (let [ind (util/read-ind (count code)) , [pool, e] (util/solve pool e ind)]
                                                     (recur e pool (inc cpt)))))))
      :else (println "Argument invalide : " (first args) "; choisir un mode entre 1 (jeu), 2 (solveur) ou 3 (solveur avec indications manuelles) .."))
    (println "Argument requis; choisir un mode entre 1 (jeu), 2 (solveur auto) ou 3 (solveur avec indications manuelles).")))
