(ns projet1.util)
(require '[clojure.string :as str])

(defn rand-color
  "Renvoie une couleur aléatoire parmis rouge, bleu, vert, jaune, noir et blanc."
  []
  (rand-nth [:rouge :bleu :vert :jaune :noir :blanc]))

(defn code-secret
  "Renvoie un code composé de n couleurs"
  [n]
  (loop [n n, res []]
    (if (zero? n)
      res
      (recur (dec n) (conj res (rand-color))))))

(defn indications
  "Renvoie une indication sur la validiaté de la tentative de résolution du code : 
:good pour une couleur bien placée, :color pour une couleur mal placée mais présente, :bad pour une couleur absente"
  [solution essai]
  (loop [sol solution, essai essai, res []]
    (if (and (seq sol) (seq essai))
      (if (= (first sol) (first essai))
        (recur (rest sol) (rest essai) (conj res :good))
        (if (some #{(first essai)} solution)
          (recur (rest sol) (rest essai) (conj res :color))
          (recur (rest sol) (rest essai) (conj res :bad))))
      res)))

(defn frequences
  "Renvoie une map dont les clés sont des éléments du vecteur v et les valeurs leur nombre d'apparition."
  [v]
  (loop [v v, res {}]
    (if (seq v)
      (recur (rest v) (if (contains? res (first v))
                        (assoc res (first v) (inc (get res (first v))))
                        (assoc res (first v) 1)))
      res)))

(defn freqs-dispo
  "Renvoie une la map des fréquences disponibles dans le code par rapport aux indications."
  [code indications]
  (loop [code code, ind indications, res {}]
    (if (seq code)
      (recur (rest code) (rest ind) (if (not (= (first ind) :good))
                                      (if (contains? res (first code))
                                        (assoc res (first code) (inc (get res (first code))))
                                        (assoc res (first code) 1))
                                      (if (contains? res (first code))
                                        res
                                        (assoc res (first code) 0))))
      res)))

(defn filtre-indications
  "Renvoie une indication sur le code à partir d'une précédente indication en appliquant la règle de multiplicité."
  [code essai indication]
  (loop [c code, e essai, ind indication, freq-disp (freqs-dispo code ind), res []]
    (if (seq c)
      (if (or (nil? (get freq-disp (first e))) (zero? (get freq-disp (first e))))
        (recur (rest c) (rest e) (rest ind) freq-disp (conj res (if (= (first ind) :good)
                                                                  :good
                                                                  :bad)))
        (recur (rest c) (rest e) (rest ind) (if (or (= (first ind) :good) (= (first ind) :bad))
                                              freq-disp
                                              (assoc freq-disp (first e) (dec (get freq-disp (first e))))) (conj res (first ind))))
      (frequences res))))

(defn couleur-valide?
  "Vérifie qu'un mot soit une couleur valide."
  [mot]
  (or (= mot "rouge") (= mot "bleu") (= mot "vert") (= mot "jaune") (= mot "noir") (= mot "blanc")))

(defn convert-vec-string-kw
  "Convertit un vecteur de string en vecteur de keywords."
  [v]
  (loop [v v, res []]
    (if (seq v)
      (recur (rest v) (conj res (keyword (first v))))
      res)))

(defn read-code
  "Renvoie un code lu dans le terminal."
  [taille]
  (println "Entrer un code :")
  (let [ln (read-line)]
    (loop [ln (str/split ln #" "), res []]
      (if (seq ln)
        (recur (rest ln) (if (couleur-valide? (first ln))
                           (do
                             (println "Mot accepté : " (first ln))
                             (conj res (first ln)))
                           (do
                             (println "Mot rejeté : " (first ln))
                             res)))
        (if (or (= (count res) taille) (= taille 0))
          (do
            (println "Code : " res)
            (convert-vec-string-kw res))
          (do
            (println "Nombre de mots incomptable :" taille "mots requis. Entrez un code :")
            (recur (str/split (read-line) #" ") [])))))))

(defn read-int
  "Retourne un int entré dans un terminal."
  []
  (Integer/parseInt (read-line)))

(defn read-ind
  "..."
  [taille]
  (loop [res {}]
    (println "Entrer une indication :")
    (println "Nombre de good :")
    (let [g (read-int), res (if (not= g 0)
                              (assoc res :good g)
                              res)]
      (println "Nombre de color :")
      (let [c (read-int), res (if (not= c 0)
                                (assoc res :color c)
                                res), res (if (not= (+ g c) taille)
                                            (assoc res :bad (- taille (+ g c)))
                                            res)]
        (println "Indication entrée : " res)
        (if (or (< g 0) (< c 0) (< taille (+ g c)))
          (do
            (println "Nombres invalides.")
            (recur {}))
          res)))))

(defn next-color
  "Renvoie une couleur selon un cylce en fonction de celle passée en argument."
  [clr]
  (get {:rouge :bleu, :bleu :vert, :vert :jaune, :jaune :noir, :noir :blanc, :blanc :rouge} clr))

(defn same-ind?
  "Compare l'indication en paramètre à l'indication qui serait donné au test par rapport à l'étalon."
  [test etalon ind]
  (= (filtre-indications etalon test (indications etalon test)) ind))

(defn solve
  "Prend en paramètre un pool de solutions potnetielles, l'essai précédent et l'indication correspondante;
Renvoie le pool filtré et un essai aléatoire au sein de ce pool."
  [pool essai indication]
  (let [newpool (filter (fn [x] (same-ind? x essai indication)) pool)]
    [newpool, (rand-nth newpool)]))

(defn newpool
  "Renvoie un pool de tous les codes possibles de taille n."
  [n]
  (loop [cpt 0, tmp [], pool [[]]]
    (if (< cpt n)
      (if (seq pool)
        (recur cpt (conj tmp (conj (first pool) :rouge)
                         (conj (first pool) :bleu)
                         (conj (first pool) :vert)
                         (conj (first pool) :jaune)
                         (conj (first pool) :noir)
                         (conj (first pool) :blanc)) (rest pool))
        (recur (inc cpt) [] tmp))
      pool)))