(ns projet1.util-test
  (:require [clojure.test :refer :all]))
(require '[projet1.util :as util])

(deftest rand-color-test
  (testing "Les couleurs sont-elles correctes ?"
    (is (contains? #{:rouge :bleu :vert :jaune :noir :blanc} (util/rand-color))))
  (testing "Les couleurs semblent-elles aléatoires ?"
    (is (> (count (filter true? (map not=
                                     (repeatedly 20 util/rand-color)
                                     (repeatedly 20 util/rand-color))))
           0))))

(deftest code-secret-test
  (testing "Le code est-il de la bonne taille ?"
    (is (= 6 (count (util/code-secret 6)))))
  (testing "Le code est-il composé de couleurs valides ?"
    (is (reduce (fn [x y] (and x y)) (map #(contains? #{:rouge :bleu :vert :jaune :noir :blanc} %) (util/code-secret 6))))))

(deftest indications-test
  (testing "L'indication donnée est-elle valide ?"
    (is (and 
         (= (util/indications [:rouge :vert :bleu :rouge] [:rouge :rouge :vert :noir]) [:good :color :color :bad])
         (= (util/indications [:rouge :vert :bleu :rouge] [:rouge :vert :bleu :rouge]) [:good :good :good :good])
         (= (util/indications [:rouge :vert :bleu :rouge] [:noir :blanc :noir :blanc]) [:bad :bad :bad :bad])))))

(deftest frequences-test
  (testing "Les fréquences renvoyées sont-elles correctes ?"
    (is (and 
         (= (util/frequences [:rouge :rouge :bleu :bleu :rouge :jaune]) {:rouge 3, :bleu 2, :jaune 1})
         (= (util/frequences []) {})
         (= (util/frequences [:test]) {:test 1})))))

(deftest freqs-dispo-test
  (testing "Les fréquences disponibles renvoyées sont-elles correctes ?"
    (is (and
         (= (util/freqs-dispo [:rouge :vert :bleu :rouge] [:good :color :color :bad]) {:rouge 1, :vert 1, :bleu 1})
         (= (util/freqs-dispo [:rouge :vert :bleu :rouge] [:good :good :good :good]) {:rouge 0, :vert 0, :bleu 0})
         (= (util/freqs-dispo [:rouge :vert :bleu :rouge] [:bad :bad :bad :bad]) {:rouge 2, :vert 1, :bleu 1})))))

(deftest filtre-indications-test
  (testing "Les indications renvoyées sont-elles correctes ?"
    (is (and
         (= (util/filtre-indications [:rouge :vert :bleu :rouge] [:rouge :rouge :vert :noir] [:good :color :color :bad]) {:good 1, :color 2, :bad 1})
         (= (util/filtre-indications [:rouge :vert :bleu :rouge] [:rouge :vert :bleu :rouge] [:good :good :good :good]) {:good 4})
         (= (util/filtre-indications [:rouge :vert :bleu :rouge] [:noir :blanc :noir :blanc] [:bad :bad :bad :bad]) {:bad 4})))))

(deftest couleur-valide-test
  (testing "La fonction filtre-elle correctement les couleurs ?"
    (is (and
         (util/couleur-valide? "rouge")
         (not (util/couleur-valide? "carmin"))))))

(deftest convert-vec-string-kw-test
  (testing "Les string du vecteur sont-elles bien converties en keywords ?"
    (is (and
         (= (util/convert-vec-string-kw ["rouge" "vert" "bleu" "rouge"]) [:rouge :vert :bleu :rouge])
         (= (util/convert-vec-string-kw []) [])
         (= (util/convert-vec-string-kw ["rouge" "rouge" "rouge"]) [:rouge :rouge :rouge])))))

(deftest next-color-test
  (testing "Les couleurs renvoyées sont-elles les bonnes ?"
    (is (and
         (= (util/next-color :rouge) :bleu)
         (= (util/next-color :blanc) :rouge)))))

(deftest same-ind-test
  (testing "Les indications sont-elles identiques ?"
    (is (and
         (util/same-ind? [:rouge :rouge :vert :noir] [:rouge :vert :bleu :rouge] {:good 1, :color 2, :bad 1})
         (util/same-ind? [] [] {})
         (util/same-ind? [:rouge] [:noir] {:bad 1})))))

(deftest solve-test
  (testing "Le pool renvoyé est-il correct ?"
    (is (= (first (util/solve [[:rouge :noir :bleu :vert]
                               [:rouge :vert :bleu :rouge]
                               [:noir :blanc :noir :blanc]] [:rouge :rouge :vert :noir] {:good 1, :color 2, :bad 1})) [[:rouge :noir :bleu :vert]
                                                                                                                       [:rouge :vert :bleu :rouge]]))))

(deftest newpool-test
  (testing "Le pool initial est-il correct ?"
    (is (= (util/newpool 1) [[:rouge]
                             [:bleu]
                             [:vert]
                             [:jaune]
                             [:noir]
                             [:blanc]]))))