# Mastermind Clojure

Implémentation du jeu Mastermind en Clojure. 

Projet de l'UE LU3IN020 (prog decl) de la Licence d'informatique de Sorbonne Université (2019/2020).

## Instructions

Lancer le programme avec la commande lein run et un argument parmis "1", "2" ou "3" selon le mode souhaité. 

### Mode 1 : Joueur

En mode joueur, entrer la taille de code souhaitée, puis entrer un code sur une seule ligne (exemple : rouge bleu rouge vert). 

### Mode 2 : Solveur auto

En mode solveur auto, entrer un code sur une seule ligne; le solveur commencera par faire une proposition aléatoire, calculera les indications, et adaptera ses propositions suivantes en fonction. 

### Mode 3 : Solveur manuel

En mode solveur manuel, entrer un code sur une seule ligne; quand le solveur fait une proposition, entrer les indications : d'abord le nombre de "good" (couleurs présentes au bon emplacement), puis le nombre de "color" (couleurs présentes au mauvais emplacement). 